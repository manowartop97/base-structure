<?php

namespace Manowartop\BaseRepositoryAndService;

use Illuminate\Support\ServiceProvider;

/**
 * Class BaseRepositoryAndServiceServiceProvider
 */
class BaseRepositoryAndServiceServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {

    }

    /**
     * @return void
     */
    public function boot(): void
    {

    }
}
