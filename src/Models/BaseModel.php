<?php

namespace Manowartop\BaseRepositoryAndService\Models;

use Illuminate\Database\Eloquent\Model;
use Manowartop\BaseRepositoryAndService\Models\Contracts\BaseModelEntityInterface;

/**
 * Class BaseModel
 * @package manowartop\BaseRepositoryAndService\Models
 */
class BaseModel extends Model implements BaseModelEntityInterface
{
    /**
     * @return string
     */
    public static function getTableName():string
    {
        return (new static())->getTable();
    }
}
